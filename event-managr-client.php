<?php
/*
Plugin Name: Event Managr Client
Version: 0.0.1
Description: Helps to administer and organize a Event with volunteers, workshops and room services
Author: agilevolunteer
Author URI: http://agilevolunteer.com/uber-mich/
Plugin URI: http://agilevolunteer.com/uber-mich/
Text Domain: wp-event-managr-client
Domain Path: /languages
*/


include(plugin_dir_path(__FILE__)."WorkshopsPosttype.php");
add_action('init', 'AgvRegisterPlaceAndTimeTaxonomy', 0);
add_action('init', 'AgvRegisterWorkshopPosttype', 0);
add_action('the_post', 'custom_post');
add_action('wp_enqueue_scripts', '_enqueueStyle');



function addCustomTermToPostType($post, $termName){
	$terms = get_the_terms($post, $termName);
	$termsContent = "<ul class='agv-terms'>";
	if (!is_array($terms) || count($terms) == 0)
		return;

	foreach($terms as $term){
		$termsContent .= "<li class='agv-terms-item'>". $term->name ."</li>";
	}
	$termsContent .= "</ul>";

	$post->post_content .= $termsContent;
}

function custom_post( $post ) {


	if($post->post_type = "workshops"){
		addCustomTermToPostType($post, "times");
		addCustomTermToPostType($post, "places");
		addCustomTermToPostType($post, "methods");
	}

//	echo "<pre>";
////	print_r(get_the_terms($post, "times"));
//	print_r($post);
//	echo "</pre>";
}

function _enqueueStyle(){
	wp_enqueue_style('event-managr', plugins_url('/styles/event-managr.css', __FILE__) );

}
?>