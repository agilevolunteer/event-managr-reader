<?php
//TODO: Place it in a class
// Register Custom Post Type

function AgvRegisterWorkshopPosttype() {
    $labels = array(
        'name' => 'workshop',
        'singular_name' => 'Workshop',
        'menu_name' => 'Workshops',
        'parent_item_colon' => 'Parent Item:',
        'all_items' => 'Alle Workshops',
        'view_item' => 'Workshop anzeigen',
        'add_new_item' => 'Neuen Workshop hinzufügen',
        'add_new' => 'Neuer Workshop',
        'edit_item' => 'Workshop bearbeiten',
        'update_item' => 'Workshop aktualisieren',
        'search_items' => 'Workshop suchen'

    );
    $capabilities = array(
        'edit_post' => 'edit_workshops',
        'read_post' => 'read_workshops',
        'delete_post' => 'delete_workshops',
        'edit_posts' => 'edit_workshopss',
        'edit_others_posts' => 'edit_others_workshopss',
        'publish_posts' => 'publish_workshops',
        'read_private_posts' => 'read_private_workshops'
    );
    $args = array(
        'label' => 'workshops',
        'description' => 'Workshops Beschreibung',
        'labels' => $labels,
        'supports' => array( 'title', 'editor', 'author', 'custom-fields', ),
        'taxonomies' => array( 'times', 'places' ),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'menu_icon' => '',
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capabilities' => $capabilities, );

    register_post_type( 'workshops', $args );
    AgvRegisterWorkshopCapabilities();
}

function AgvRegisterWorkshopCapabilities() {
    $role = get_role( 'administrator' );
    $role->add_cap( 'edit_workshops' );
    $role->add_cap( 'edit_workshopss' );
    $role->add_cap( 'edit_others_workshopss' );
    $role->add_cap( 'publish_events' );
    $role->add_cap( 'read_workshops' );
    $role->add_cap( 'read_private_events' );
    $role->add_cap( 'delete_workshops' );
    $role->add_cap( 'publish_workshops' );
    $role->add_cap( 'assign_workshops_terms' );
}

// Register Custom Taxonomy

function AgvRegisterPlaceAndTimeTaxonomy() {
    $labelsPlace = array(
        'name'                       => _x( 'Ort', 'Taxonomy General Name', 'x7Workshops' ),
        'singular_name'              => _x( 'Orte', 'Taxonomy Singular Name', 'x7Workshops' ),
        'menu_name'                  => __( 'Orte', 'x7Workshops' ),
        'all_items'                  => __( 'Alle Orte', 'x7Workshops' ),
        'parent_item'                => __( 'Parent Item', 'x7Workshops' ),
        'parent_item_colon'          => __( 'Parent Item:', 'x7Workshops' ),
        'new_item_name'              => __( 'Neuer Ort', 'x7Workshops' ),
        'add_new_item'               => __( 'Neuen Ort hinzufÃ¼gen', 'x7Workshops' ),
        'edit_item'                  => __( 'Ort bearbeiten', 'x7Workshops' ),
        'update_item'                => __( 'Ort aktualisieren', 'x7Workshops' ),
        'separate_items_with_commas' => __( 'Separate items with commas' ),
        'search_items'               => __( 'Orte suchen', 'x7Workshops' ),
        'add_or_remove_items'        => __( 'Orte hinzufÃ¼gen oder lÃ¶schen', 'x7Workshops' ),
        'choose_from_most_used'      => __( 'Aus den hÃ¤ufig genutzten Orten auswÃ¤hlen' ),
        'not_found'                  => __( 'Keine Orte gefunden')
    );

    $labelsTime = array(
        'name'                       => _x( 'Zeiten', 'Taxonomy General Name', 'x7Workshops' ),
        'singular_name'              => _x( 'Zeit', 'Taxonomy Singular Name', 'x7Workshops' ),
        'menu_name'                  => __( 'Zeiten', 'x7Workshops' ),
        'all_items'                  => __( 'Zeiten', 'x7Workshops' ),
        'parent_item'                => __( 'Parent Item', 'x7Workshops' ),
        'parent_item_colon'          => __( 'Parent Item:', 'x7Workshops' ),
        'new_item_name'              => __( 'Neuer Zeitpunkt', 'x7Workshops' ),
        'add_new_item'               => __( 'Neuen Zeitpunkt hinzufÃ¼gen', 'x7Workshops' ),
        'edit_item'                  => __( 'Zeitpunkt bearbeiten', 'x7Workshops' ),
        'update_item'                => __( 'Zeitpunkt aktualisieren', 'x7Workshops' ),
        'add_or_remove_items'        => __( 'Zeiten hinzufÃ¼gen oder lÃ¶schen', 'x7Workshops' ),
        'choose_from_most_used'      => __( 'Aus den hÃ¤ufig genutzten Zeiten auswÃ¤hlen' ),
        'not_found'                  => __( 'Keine Zeiten gefunden')

    );

    $labelsEquipment = array(
        'name'                       => _x( 'Equipments', 'Taxonomy General Name', 'x7Workshops' ),
        'singular_name'              => _x( 'Equiment', 'Taxonomy Singular Name', 'x7Workshops' ),
        'menu_name'                  => __( 'Equipment', 'x7Workshops' ),
        'all_items'                  => __( 'Equipments', 'x7Workshops' ),
        'parent_item'                => __( 'Parent Item', 'x7Workshops' ),
        'parent_item_colon'          => __( 'Parent Item:', 'x7Workshops' ),
        'new_item_name'              => __( 'Neues Equipment', 'x7Workshops' ),
        'add_new_item'               => __( 'Neues Equipment hinzufügen', 'x7Workshops' ),
        'edit_item'                  => __( 'Equipment bearbeiten', 'x7Workshops' ),
        'update_item'                => __( 'Equipment aktualisieren', 'x7Workshops' ),
        'add_or_remove_items'        => __( 'Equipment hinzufügen oder löschen', 'x7Workshops' ),
        'choose_from_most_used'      => __( 'Aus den häufig genutzten Equipments auswählen' ),
        'not_found'                  => __( 'Kein Equipment gefunden')
    );

    $labelsMethod = array(
        'name'                       => _x( 'Methoden', 'Taxonomy General Name', 'x7Workshops' ),
        'singular_name'              => _x( 'Methode', 'Taxonomy Singular Name', 'x7Workshops' ),
        'menu_name'                  => __( 'Methoden', 'x7Workshops' ),
        'all_items'                  => __( 'Methoden', 'x7Workshops' ),
        'parent_item'                => __( 'Parent Item', 'x7Workshops' ),
        'parent_item_colon'          => __( 'Parent Item:', 'x7Workshops' ),
        'new_item_name'              => __( 'Neue Methode', 'x7Workshops' ),
        'add_new_item'               => __( 'Neue Methode hinzufügen', 'x7Workshops' ),
        'edit_item'                  => __( 'Methode bearbeiten', 'x7Workshops' ),
        'update_item'                => __( 'Methode aktualisieren', 'x7Workshops' ),
        'add_or_remove_items'        => __( 'Methode hinzufügen oder löschen', 'x7Workshops' ),
        'choose_from_most_used'      => __( 'Aus den häufig genutzten Methoden auswählen' ),
        'not_found'                  => __( 'Keine Methode gefunden')
    );

    $labelsTopic = array(
        'name'                       => _x( 'Themengebiete', 'Taxonomy General Name', 'x7Workshops' ),
        'singular_name'              => _x( 'Themengebiet', 'Taxonomy Singular Name', 'x7Workshops' ),
        'menu_name'                  => __( 'Themengebiete', 'x7Workshops' ),
        'all_items'                  => __( 'Themengebiete', 'x7Workshops' ),
        'parent_item'                => __( 'Parent Item', 'x7Workshops' ),
        'parent_item_colon'          => __( 'Parent Item:', 'x7Workshops' ),
        'new_item_name'              => __( 'Neues Themengebiet', 'x7Workshops' ),
        'add_new_item'               => __( 'Neues Themengebiet hinzufügen', 'x7Workshops' ),
        'edit_item'                  => __( 'Themengebiet bearbeiten', 'x7Workshops' ),
        'update_item'                => __( 'Themengebiet aktualisieren', 'x7Workshops' ),
        'add_or_remove_items'        => __( 'Themengebiet hinzufügen oder löschen', 'x7Workshops' ),
        'choose_from_most_used'      => __( 'Aus den häufig genutzten Themengebieten auswählen' ),
        'not_found'                  => __( 'Kein Themengebiet gefunden')
    );

    $capabilities = array(
        'manage_terms'               => 'edit_workshopss',
        'edit_terms'                 => 'edit_workshopss',
        'delete_terms'               => 'edit_workshopss',
        'assign_terms'               => 'assign_workshops_terms'
    );
    $argsPlace = array(
        'labels'                     => $labelsPlace,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'capabilities'               => $capabilities,
    );

    $argsTime = array(
        'labels'                     => $labelsTime,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'capabilities'               => $capabilities,
    );

    $argsEquipment = array(
        'labels'                     => $labelsEquipment,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'capabilities'               => $capabilities,
    );

    $argsMethod = array(
        'labels'                     => $labelsMethod,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'capabilities'               => $capabilities,
    );

    $argsTopic = array(
        'labels'                     => $labelsTopic,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'capabilities'               => $capabilities,
    );

    /*echo "<pre>";
    print_r($argsEquipment);
    echo "</pre";
*/
    register_taxonomy( 'places', array( 'workshops' ),  $argsPlace);
    register_taxonomy( 'times', array( 'workshops' ), $argsTime );
    register_taxonomy( 'equipment', array( 'workshops' ), $argsEquipment );
    register_taxonomy( 'methods', array( 'workshops' ), $argsMethod );
    register_taxonomy( 'topics', array( 'workshops' ), $argsTopic );


}



add_action('json_insert_post', 'agv_insert_taxonomies_for_post', 10, 3);
function agv_insert_taxonomies_for_post($post, $data, $update){

    $terms = $data["terms"];

    $taxes = array_keys($terms);

    for($i = 0; $i < count($taxes); $i++){
        $tax = $taxes[$i];
        $termsForPost = array();
        for($j = 0; $j < count($terms[$tax]); $j++){
            array_push($termsForPost, $terms[$tax][$j]["ID"]);
        }

         wp_set_post_terms( $post["ID"], $termsForPost, $tax);
    }
}


?>
